import React, { useState, useEffect, useRef } from 'react'

import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Dimensions,
  PixelRatio,
  FlatList,
  RefreshControl,
  ActivityIndicator,
  Image,
  LogBox
} from 'react-native';

import axios from 'axios'
import { getColors } from 'react-native-image-colors'

import LinearGradient from 'react-native-linear-gradient';
import { createShimmerPlaceholder } from 'react-native-shimmer-placeholder'
const ShimmerPlaceHolder = createShimmerPlaceholder(LinearGradient)

const toDp = size => {
  const screenWidth = Dimensions.get('window').width
  const elemSize = parseFloat(size)
  return PixelRatio.roundToNearestPixel(screenWidth * elemSize / 100) / 3.6
}

let limit = 0
let URL_DATA = 'https://pokeapi.co/api/v2/pokemon'
let URL_IMAGE = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/'

const { width, height } = Dimensions.get('window')
const App: () => Node = () => {

  const [state, setState] = useState({
    loading: true,
    arrayData: [],
    offset: 0,
    limit: 20,
    arrayColors: [],
    count: 0
  })

  useEffect(() => {
    loadInitial()
    LogBox.ignoreAllLogs();
  }, [])

  const loadInitial = () => {
    limit = 0
    loadData(state.offset)
    setTimeout(function () {
      setState(state => ({...state,
        loading: false
      }))
    }, 1000);
  }

  const loadData = (offset) => {
    getLoadData('?limit='+state.limit+'&offset='+offset).then(response => {
      console.log(response)
      if(offset === 0) {
        setState(state => ({...state,
          arrayData: response.data.results,
          offset: 21,
          count: response.data.count
        }))
      } else {
        setState(state => ({...state,
          arrayData: [...state.arrayData, ...response.data.results],
          offset: replaceStringOffset(response.data.next),
          count: response.data.count
        }))
      }
      loadBackgroundColor(response.data.results)
    }).catch( error => {
      console.log(error)
      setState(state => ({...state, offset: 0}))
    })
    limit = state.limit
  }

  const replaceString = (string) => {
    let s = string.replaceAll("https://pokeapi.co/api/v2/pokemon/", "")
    return s.replaceAll("/", "")
  }

  const replaceStringOffset = (string) => {
    let s = string.replaceAll("https://pokeapi.co/api/v2/pokemon?offset=", "")
    return s.replaceAll("&limit=20", "")
  }

  const loadBackgroundColor = (array) => {
    let temp = state.arrayColors
    let index = 0
    for (var i = 0; i < array.length; i++) {
      let uri = URL_IMAGE + replaceString(array[i].url)+'.png'
      console.log('uri', uri);
      getColors(uri, {fallback: '#228B22',cache: true,key: uri}).then((responseColor) => {
        let obj = {
          name: array[index].name,
          uri,
          responseColor
        }
        temp.push(obj)
        index++
        setState(state => ({...state,
          arrayColors: temp,
        }))
      })
    }
  }

  const getLoadData = (params, data) => {
    return xhr(URL_DATA + params, 'GET', data)
  }

  const xhr = async (url, method, data, headers) => {
    let defaultHeader = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    }

    let config = {
      method,
      url,
      headers: defaultHeader,
      data,
      timeout: 15000,
    }
    // LOG ALL DATA
    console.log(config)
    try {
      const res = await axios(config)
      return res
    }
    catch ({response}) {
      console.log(response);
      throw response
    }
  }

  const renderHeader = () => {
    return (
      <View style={styles.header}>
        <Text style={styles.textHeader}>Pokedex</Text>
      </View>
    )
  }

  const renderFooter = () => {
    return (<ActivityIndicator size="large" color={'white'} style={{marginVertical: toDp(48)}} />)
  }

  const renderItemShimmer = ({item, index}) => {
    return (
      <View style={styles.touchMenu}>
        <ShimmerPlaceHolder style={styles.viewShimmer} />
        <ShimmerPlaceHolder style={styles.textShimmer} />
      </View>
    )
  }

  const renderItem = ({item, index}) => {
    let uri = URL_IMAGE + (index+1) + '.png'
    let backgroundColor = state.arrayColors[index]?.responseColor.background
    return (
      <View style={[styles.touchMenu, {backgroundColor}]}>
        <Image
          source={{uri}}
          style={styles.imageItem}
        />
        <View style={styles.viewTextPosition}>
          <Text
            numberOfLines={1}
            ellipsizeMode='tail'
            style={styles.textTitleMenu}
          >
            {item.name}
          </Text>
        </View>
      </View>
    )
  }

  const renderContent = () => {
    return (
      <View style={styles.content}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          removeClippedSubviews={true}
          refreshControl={
            <RefreshControl
              refreshing={state.loading}
              onRefresh={() => {
                setState(state => ({...state,
                  loading: false,
                  arrayData: [],
                  offset: 0,
                  limit: 20,
                  arrayColors: [],
                  count: 0
                }))
                loadData(0)
              }}
            />
          }
          onMomentumScrollEnd={(e) => {
            let hasil = limit - parseInt(e.nativeEvent.contentOffset.y / toDp(140))
            console.log('hasil', hasil);
            if((hasil <= 20) && state.offset < state.count) {
              let offset = parseInt(state.offset) + parseInt(state.limit)
              loadData(offset)
              limit += state.limit
            }
          }}
        >
          <FlatList
            data={state.loading ? ['','','','','','','',''] : state.arrayData}
            renderItem={state.loading ? renderItemShimmer : renderItem}
            numColumns={2}
            ListFooterComponent={state.offset < state.count ? renderFooter() : <View style={{height: toDp(48)}} />}
          />
        </ScrollView>
      </View>
    )
  }

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle={'light-content'} />
      {renderHeader()}
      {renderContent()}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#2B292C'
  },
  header: {
    backgroundColor: '#D53B47',
    width: '100%',
    height: toDp(56),
    justifyContent: 'center',
    paddingHorizontal: toDp(16)
  },
  textHeader: {
    fontSize: toDp(20),
    color: 'white',
    fontWeight: 'bold'
  },
  content: {
    padding: toDp(8),
    width,
  },
  touchMenu: {
    marginLeft: toDp(2),
    marginRight: toDp(8),
    marginBottom: toDp(8),
    shadowColor: "#000",
    shadowOffset: {
    	width: 0,
    	height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 2,
    alignItems: 'center',
    width: toDp(164),
    height: 'auto',
    borderRadius: toDp(8),
    backgroundColor: 'white'
  },
  viewShimmer: {
    width: toDp(164),
    height: toDp(140),
    resizeMode: 'cover',
    borderTopRightRadius: toDp(8),
    borderTopLeftRadius: toDp(8),
  },
  imageItem: {
    width: toDp(164),
    height: toDp(140),
    borderTopRightRadius: toDp(8),
    borderTopLeftRadius: toDp(8),
  },
  textShimmer: {
    marginVertical: toDp(8),
    width: toDp(124),
    borderRadius: toDp(2)
  },
  viewTextPosition: {
    width: '100%',
    marginVertical: toDp(8),
  },
  textTitleMenu: {
    marginHorizontal: toDp(8),
    fontSize: toDp(14),
    color: '#FFFFFF',
    textAlign: 'center',
    fontWeight: 'bold'
  },
});

export default App;
